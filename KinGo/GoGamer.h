#pragma once
#include "GoGamer.hpp"
#include <atomic>
#include <memory>
#include <KinBase\CallBackFunction.h>
namespace KinGo
{
	class GoPoint
	{
	public:
		int8_t x;
		int8_t y;
		bool IsValid(uint8_t max)
		{
			return (x >= 0 && x < max) && (y >= 0 && y < max);
		}

		void MakeResign()
		{

		}
		bool IsResign()
		{

		}
	};

	enum class PlayerColor
	{
		Black = 1,
		White = 2
	};

	class GoGamer
	{
	public:
		GoGamer() = default;
		virtual ~GoGamer() = default;

		virtual void Play(GoPoint pt) = 0;
		virtual void Play(GoPoint pt, PlayerColor Color) {};
		virtual void Play() = 0;

	};
}

