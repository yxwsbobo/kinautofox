#include<Windows.h>
#include <TlHelp32.h>
#include <string>

std::string GetModuleDir(std::string Name = "")
{
	char Dir[MAX_PATH];
	HMODULE Module;
	if (Name.empty())
	{
		Module = nullptr;
	}
	else
	{
		Module = GetModuleHandleA(Name.c_str());
	}
	GetModuleFileNameA(Module, Dir, MAX_PATH);

	std::string Result{ Dir };
	Result = Result.substr(0, Result.find_last_of('\\')+1);
	return Result;
}

std::string WindowTittle = "KinAutoFoxTittle";
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	auto h = FindWindowExA(HWND_MESSAGE, 0, NULL, WindowTittle.c_str());
	if (h)
	{
		MessageBoxA(0, 0, "KinAutoFox �Ѿ�����", 0);
		return 0;
	}

	DWORD dwPID = 0;
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (INVALID_HANDLE_VALUE == hSnapshot) {
		MessageBoxA(0, 0, "CreateToolhelp32Snapshot Failed", 0);
		return 0;
	}
	PROCESSENTRY32 pe;
	pe.dwSize = sizeof(pe);
	for (BOOL ret = Process32First(hSnapshot, &pe); ret; ret = Process32Next(hSnapshot, &pe)) {
		std::wstring FileName{ pe.szExeFile };
		if (FileName.find(L"foxwq.exe", 0) != std::wstring::npos)
		{
			dwPID = pe.th32ProcessID;
			break;
		}
	}
	CloseHandle(hSnapshot);

	if (!dwPID)
	{
		MessageBoxA(0, 0, "Can't Find foxwq.exe", 0);
		return 0;
	}

	auto szDllPath = GetModuleDir() + "KinAutoFox.dll";

	HANDLE hProcess = NULL;
	HANDLE hThread = NULL;
	HMODULE hMod = NULL;
	LPVOID pRemoteBuf = NULL;
	LPTHREAD_START_ROUTINE pThreadProc;

	if (!(hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwPID))) {
		MessageBox(0, L"OpenProcess Failed", L"", 0);
		return 0;
	}

	pRemoteBuf = VirtualAllocEx(hProcess, NULL, szDllPath.size() + 1, MEM_COMMIT, PAGE_READWRITE);

	WriteProcessMemory(hProcess, pRemoteBuf, (LPVOID)szDllPath.c_str(), szDllPath.size() + 1, NULL); 

	hMod = GetModuleHandleA("kernel32.dll");
	pThreadProc = (LPTHREAD_START_ROUTINE)GetProcAddress(hMod, "LoadLibraryA");  

	hThread = CreateRemoteThread(hProcess, NULL, 0, pThreadProc, pRemoteBuf, 0, NULL);

	WaitForSingleObject(hThread, INFINITE);
	CloseHandle(hThread);
	CloseHandle(hProcess);

	return 0;
}